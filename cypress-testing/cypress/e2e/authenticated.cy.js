/// <reference types="cypress" />

const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImR1bW15MjYiLCJfaWQiOiI2MzMyOWY2Y2ZlYjMyOTAwMDlkMjIzY2IiLCJuYW1lIjoiZHVtbXkiLCJpYXQiOjE2NjQyNjE5OTgsImV4cCI6MTY2OTQ0NTk5OH0.DP28eBFQP0Ml09v7nHbLouw8t9TzHyePObD9J3XgBo8"

describe('Basic Authentication Desktop Tests', () => {
  before(() => {
    
    cy.then(() => {
      window.localStorage.setItem('__auth__token', token)
    })
  })
        
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit('https://codedamn.com');
  });

 it('Should load playground correctly', () => {
    cy.visit("https://codedamn.com/playgrounds");
    cy.contains("HTML/CSS").click();
    cy.wait(1000)
    cy.contains('div', 'Create Playground').find('button').first().click()
    cy.get('div');
    cy.debug();
 })
});
