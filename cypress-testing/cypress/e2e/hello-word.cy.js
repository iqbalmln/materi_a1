/// <reference types="cypress" />

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImR1bW15MjYiLCJfaWQiOiI2MzMyOWY2Y2ZlYjMyOTAwMDlkMjIzY2IiLCJuYW1lIjoiZHVtbXkiLCJpYXQiOjE2NjQyNjE5OTgsImV4cCI6MTY2OTQ0NTk5OH0'

describe('Basic Test', () => {
  // it('The webpage loads, at least', () => {
  //   cy.visit('https://codedamn.com')

  //   // should has mocha library syntax
  //   // Way 1
  //   // cy.contains('Learn Programming').should('exist')

  //   // Way 2
  //   // cy.get('.cd-morph-dropdown > div > a')

  //   // Way 3 / Best Way
  //   // cy.get('[data-bypassmenuclose=true]').click()
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit('https://codedamn.com');
  });
  // } )
  it('Login page looks good', () => {

    cy.contains('Sign in').click()
    cy.contains('Forgot your password?').should('exist')
    cy.contains('Remember me').should('exist')
    cy.contains('Create one').should('exist')

  })
  it('Link on login page work as fine', () => {

    cy.contains('Sign in').click()
    cy.contains('Forgot your password').click({force: true})
    cy.url().should('include', '/password-reset')
    cy.go('back')
    cy.contains('Create one').click()
    cy.url().should('include', '/register')

  })
  it.only('Login should display correct error work fine', () => {
    cy.contains('Sign in').click()
    
    cy.contains('Sign in to codedamn').should('exist')
    cy.get('[data-testid=username]').type('admin',{force: true})
    cy.get('[data-testid=password]').type('admin',{force: true})

    cy.get('[data-testid=login]').click({force: true})
  })
  // it('Every basic element exist on mobile', () => {
  //   // cy viewport
  //   cy.viewport('iphone-8')
  //   cy.visit('https://codedamn.com')

  // } )
})